![Logo](Docs/logo.png)

# SmartNum

[![Version](https://img.shields.io/badge/Version-1.1-blue.svg?style=flat-square)](https://bitbucket.org/PiotrowskiCezary/smartnum/downloads/SmartNum.apk)
[![Unity version](https://img.shields.io/badge/Unity-2018.4%20(LTS)-lightgray.svg?style=flat-square&logo=unity)](https://unity3d.com/unity/qa/lts-releases?version=2018.4)
[![ML-Agents release](https://img.shields.io/badge/ML--Agents-Release%2012-success.svg?style=flat-square&logo=github)](https://github.com/Unity-Technologies/ml-agents/tree/release_12)


---

**SmartNum** is a 2048-type mobile game that uses artificial intelligence for player assistance, made in Unity.

---

## Download

- [APK file](https://bitbucket.org/PiotrowskiCezary/smartnum/downloads/SmartNum.apk) (SHA-256: 31126e0495f0e250ec1d1f33f9fa7d91f5e8beb41aa5c973b05f4af431927576)

---

## Installation

### Minimum requirements

- **Operating System:** Android 5.0 or later
- **Storage:** 46 MB available space

---

## Technologies used

- Unity 2019.4.18f1
- Unity ML-Agents Release 12
- Python 3.7.9
- PyTorch 1.7.0

---

## About

### 3 different boards

You can choose from three different board sizes:

- **3x3 board:** quicker games, less forgiving board
- **4x4 board:** the default game board size
- **5x5 board:** longer games, easier board

![Different boards](Docs/showcase1.gif)

### Adjustable settings

You have the ability to adjust the settings to suit your needs:

- **Music:** turn the music on and off
- **Particles:** enable or disable particle effects
- **Reset scores:** reset all your highscores
- **AI delay:** change the time delay before assisting the player

![Settings](Docs/showcase2.gif)

### AI assistant

The arrows indicate the recommended direction for the next move.

A different neural network model was trained for each board.

![AI assist](Docs/showcase3.gif)

### Controls

- **To press a button:** tap the button
- **To move blocks:** swipe left, right, up, down

![Gameplay](Docs/showcase4.gif)

---

## Example configuration file

If you want to train your own AI you can use (or tweak) the settings below.

This is the ***4x4 board*** AI training configuration file:

**config.yaml**

```yaml
behaviors:
  4x4Model:
    trainer_type: sac
    hyperparameters:
      learning_rate: 0.0003
      learning_rate_schedule: constant
      batch_size: 128
      buffer_size: 50000
      buffer_init_steps: 1000
      tau: 0.005
      steps_per_update: 10.0
      save_replay_buffer: false
      init_entcoef: 0.5
      reward_signal_steps_per_update: 10.0
    network_settings:
      normalize: false
      hidden_units: 64
      num_layers: 3
      vis_encode_type: simple
    reward_signals:
      extrinsic:
        gamma: 0.9
        strength: 1.0
    keep_checkpoints: 5
    max_steps: 4000000
    time_horizon: 5
    summary_freq: 10000
    threaded: true
```
