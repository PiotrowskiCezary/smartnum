﻿using UnityEngine;

public class FontSizeManager : MonoBehaviour
{
    public int size;
    public TextMesh textmesh;

    public void TryRescale()
    {
        size = textmesh.text.Length;
        switch (size)
        {
            case 1:
                ChangeScale(0.25f);
                break;
            case 2:
                ChangeScale(0.18f);
                break;
            case 3:
                ChangeScale(0.12f);
                break;
            case 4:
                ChangeScale(0.09f);
                break;
            case 5:
                ChangeScale(0.075f);
                break;
            case 6:
                ChangeScale(0.065f);
                break;
            case 7:
                ChangeScale(0.055f);
                break;
            case 8:
                ChangeScale(0.05f);
                break;
            default:
                ChangeScale(0.045f);
                break;
        }
    }

    void ChangeScale(float newSize)
    {
        transform.localScale = new Vector3(newSize, newSize, 1f);
    }
}
