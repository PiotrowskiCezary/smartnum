﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance = null;

    private AudioSource music;
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        music = GetComponent<AudioSource>();
        if (PlayerPrefs.HasKey("sn_mute"))
        {
            if (PlayerPrefs.GetInt("sn_mute") == 1)
            {
                StopMusic();
            }
        }
        else
        {
            PlayerPrefs.SetInt("sn_mute", 0);
        }
    }

    public void PlayMusic()
    {
        music.mute = false;
        PlayerPrefs.SetInt("sn_mute", 0);
    }

    public void StopMusic()
    {
        music.mute = true;
        PlayerPrefs.SetInt("sn_mute", 1);
    }
}