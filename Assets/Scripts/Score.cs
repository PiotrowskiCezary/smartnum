﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public GameObject board;
    public GameObject scoreUI;
    public GameObject highScoreUI;
    public GameObject soundButton;
    public Sprite soundPlay;
    public Sprite soundStop;

    private Text scoreText;
    private Text highScoreText;

    public int boardType;
    private string highScorePref;
    private Image spriteRenderer;
    private bool muted;

    private BoardManager boardManager;

    void Start()
    {
        boardManager = board.GetComponent<BoardManager>();
        spriteRenderer = soundButton.GetComponent<Image>();
        muted = UpdateSoundSprite();
        highScoreText = highScoreUI.GetComponent<Text>();

        LoadHighScorePref();

        if (PlayerPrefs.HasKey(highScorePref))
            highScoreText.text = PlayerPrefs.GetInt(highScorePref).ToString();
    }

    void LoadHighScorePref()
    {
        if (highScorePref == null)
        {
            switch (boardType)
            {
                case 3:
                    highScorePref = "sn_highscore3";
                    break;
                case 4:
                    highScorePref = "sn_highscore4";
                    break;
                case 5:
                    highScorePref = "sn_highscore5";
                    break;
                default:
                    highScorePref = "sn_highscore4";
                    break;
            }
        }
    }

    void Save()
    {
        int total = 0;
        string key;
        foreach (int value in boardManager.valueList)
        {
            key = "sn_value" + total;
            PlayerPrefs.SetInt(key, value);
            total++;
        }
        PlayerPrefs.SetInt("sn_size", boardManager.size);
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Save();
        }
    }

    public void SetScore(int score)
    {
        if (scoreText == null)
            scoreText = scoreUI.GetComponent<Text>();
        if (highScoreText == null)
            highScoreText = highScoreUI.GetComponent<Text>();

        scoreText.text = score.ToString();

        LoadHighScorePref();
        if (score > PlayerPrefs.GetInt(highScorePref))
        {
            PlayerPrefs.SetInt(highScorePref, score);
            highScoreText.text = score.ToString();
        }
    }

    public void ToggleMusic()
    {
        GameObject music = GameObject.Find("Music");
        SoundManager soundManager = music.GetComponent<SoundManager>();
        if (UpdateSoundSprite())
        {
            soundManager.PlayMusic();
        }
        else
        {
            soundManager.StopMusic();
        }
        muted = UpdateSoundSprite();
    }

    public bool UpdateSoundSprite()
    {
        if (PlayerPrefs.GetInt("sn_mute") == 1)
        {
            spriteRenderer.overrideSprite = soundStop;
            return true;
        }
        else
        {
            spriteRenderer.overrideSprite = null;
            return false;
        }
    }

    public void Options()
    {
        if (boardManager.playerTurn == true)
        {
            boardManager.playerTurn = false;
            Save();
            SceneManager.LoadScene("Menu");
        }
    }
}
