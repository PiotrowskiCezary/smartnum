﻿using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public GameObject spawnObject;
    public int size = 3;
    public int[,] valueList;
    public GameObject[,] objectList;
    public bool playerTurn = false;
    public float verticalInput;
    public float horizontalInput;
    public float turnDelay = 0.6f;
    public int score = 3;
    public GameObject scoreNumber;
    public GameObject gameOverOverlay;
    public GameObject agent;
    public int action = 4;
    public GameObject arrows;
    public float arrowsDelay = 3f;
    public bool enableArrows = true;
    public Swipe swipeControls;

    private AIAgent ai;
    
    void Start()
    {
        valueList = new int[size, size];
        objectList = new GameObject[size, size];
        ZeroList();
        FillValueList();
        FillObjectList();
        CheckSave();
        CalculateScore();
        LoadPrefs();
        ai = agent.GetComponent<AIAgent>();
        ai.RequestDecision();
        playerTurn = true;
    }
    
    void Update()
    {
    #if UNITY_STANDALONE || UNITY_EDITOR
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.F))
            Logging();
    #else
        if (swipeControls.SwipeLeft)
            horizontalInput = -1f;
        if (swipeControls.SwipeRight)
            horizontalInput = 1f;
        if (swipeControls.SwipeDown)
            verticalInput = -1f;
        if (swipeControls.SwipeUp)
            verticalInput = 1f;
    #endif

        if (playerTurn)
        {
            // Left
            if (horizontalInput < 0)
            {
                if (CheckLeft())
                {
                    playerTurn = false;
                    CancelInvoke("ArrowsOn");
                    ToggleArrows(false);
                    MoveLeft();
                    Invoke("SetTurn", turnDelay);
                }
            }
            // Right
            else if (horizontalInput > 0)
            {
                if (CheckRight())
                {
                    playerTurn = false;
                    CancelInvoke("ArrowsOn");
                    ToggleArrows(false);
                    MoveRight();
                    Invoke("SetTurn", turnDelay);
                }
            }
            // Down
            else if (verticalInput < 0)
            {
                if (CheckDown())
                {
                    playerTurn = false;
                    CancelInvoke("ArrowsOn");
                    ToggleArrows(false);
                    MoveDown();
                    Invoke("SetTurn", turnDelay);
                }
            }
            // Up
            else if (verticalInput > 0)
            {
                if (CheckUp())
                {
                    playerTurn = false;
                    CancelInvoke("ArrowsOn");
                    ToggleArrows(false);
                    MoveUp();
                    Invoke("SetTurn", turnDelay);
                }
            }
            horizontalInput = 0f;
            verticalInput = 0f;
        }
    }

    void CreateNewBlock()
    {
        Vector3 pick = PickRandom();
        GameObject newBlock = Instantiate(spawnObject, pick, Quaternion.identity) as GameObject;
        objectList[(int)pick.x, (int)pick.y] = newBlock;
        valueList[(int)pick.x, (int)pick.y] = newBlock.GetComponent<Block>().value;
    }

    void CreateNewBlock(int value, int x, int y)
    {
        Vector3 pick = new Vector3(x, y, 0f);
        GameObject newBlock = Instantiate(spawnObject, pick, Quaternion.identity) as GameObject;
        newBlock.GetComponent<Block>().value = value;
        objectList[x, y] = newBlock;
        valueList[x, y] = value;
    }

#if UNITY_STANDALONE || UNITY_EDITOR
    void Logging()
    {
        System.Text.StringBuilder text = new System.Text.StringBuilder();
        for (int i = size - 1; i >= 0; i--)
        {
            for (int j = 0; j < size; j++)
            {
                text.Append(valueList[j, i].ToString() + ", ");
            }
            text.Append("\n");
        }
        Debug.Log(text);
    }
#endif

    void ZeroList()
    {
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                valueList[i, j] = 0;
    }

    void FillValueList()
    {
        GameObject[] list = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                foreach (GameObject o in list)
                {
                    if (i == o.transform.position.x && j == o.transform.position.y)
                        valueList[i, j] = o.GetComponent<Block>().value;
                    else
                        valueList[i, j] = 0;
                }
            }
        }
    }

    void FillObjectList()
    {
        GameObject[] list = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                foreach (GameObject o in list)
                {
                    if (i == o.transform.position.x && j == o.transform.position.y)
                        objectList[i, j] = o;
                    else
                        objectList[i, j] = null;
                }
            }
        }
    }

    Vector3 PickRandom()
    {
        List<Vector3> list = new List<Vector3>();
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                if (valueList[i, j] == 0)
                {
                    list.Add(new Vector3(i, j, 0f));
                }
            }
        }
        int pick = Random.Range(0, list.Count);
        return list[pick];
    }

    void SetTurn()
    {
        CreateNewBlock();
        CalculateScore();
        if (IsGameOver())
        {
            ai.AddReward(-1f);
            ai.EndEpisode();
            enableArrows = false;
            gameOverOverlay.SetActive(true);
        }
        else
        {
            ai.AddReward(0.01f);
            ai.RequestDecision();
            playerTurn = true;
        }
    }

    void MoveLeft()
    {
        for (int j = size - 1; j >= 0; j--)
        {
            for (int i = 1; i < size; i++)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i - 1, j] == 0 || valueList[i - 1, j] == valueList[i, j])
                    {
                        valueList[i - 1, j] += valueList[i, j];
                        valueList[i, j] = 0;
                        objectList[i, j].GetComponent<Block>().StartMoving(new Vector3(-1f, 0f, 0f));
                        objectList[i - 1, j] = objectList[i, j];
                        objectList[i, j] = null;
                    }
                }
            }
        }
    }

    void MoveRight()
    {
        for (int j = size - 1; j >= 0; j--)
        {
            for (int i = size - 2; i >= 0; i--)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i + 1, j] == 0 || valueList[i + 1, j] == valueList[i, j])
                    {
                        valueList[i + 1, j] += valueList[i, j];
                        valueList[i, j] = 0;
                        objectList[i, j].GetComponent<Block>().StartMoving(new Vector3(1f, 0f, 0f));
                        objectList[i + 1, j] = objectList[i, j];
                        objectList[i, j] = null;
                    }
                }
            }
        }
    }

    void MoveUp()
    {
        for (int j = size - 2; j >= 0; j--)
        {
            for (int i = 0; i < size; i++)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i, j + 1] == 0 || valueList[i, j + 1] == valueList[i, j])
                    {
                        valueList[i, j + 1] += valueList[i, j];
                        valueList[i, j] = 0;
                        objectList[i, j].GetComponent<Block>().StartMoving(new Vector3(0f, 1f, 0f));
                        objectList[i, j + 1] = objectList[i, j];
                        objectList[i, j] = null;
                    }
                }
            }
        }
    }

    void MoveDown()
    {
        for (int j = 1; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i, j - 1] == 0 || valueList[i, j - 1] == valueList[i, j])
                    {
                        valueList[i, j - 1] += valueList[i, j];
                        valueList[i, j] = 0;
                        objectList[i, j].GetComponent<Block>().StartMoving(new Vector3(0f, -1f, 0f));
                        objectList[i, j - 1] = objectList[i, j];
                        objectList[i, j] = null;
                    }
                }
            }
        }
    }

    public bool CheckLeft()
    {
        for (int j = size - 1; j >= 0; j--)
        {
            for (int i = 1; i < size; i++)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i - 1, j] == 0 || valueList[i - 1, j] == valueList[i, j])
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool CheckRight()
    {
        for (int j = size - 1; j >= 0; j--)
        {
            for (int i = size - 2; i >= 0; i--)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i + 1, j] == 0 || valueList[i + 1, j] == valueList[i, j])
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool CheckUp()
    {
        for (int j = size - 2; j >= 0; j--)
        {
            for (int i = 0; i < size; i++)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i, j + 1] == 0 || valueList[i, j + 1] == valueList[i, j])
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool CheckDown()
    {
        for (int j = 1; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (valueList[i, j] != 0)
                {
                    if (valueList[i, j - 1] == 0 || valueList[i, j - 1] == valueList[i, j])
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void CalculateScore()
    {
        score = 0;
        for (int j = size - 1; j >= 0; j--)
        {
            for (int i = 0; i < size; i++)
            {
                score += valueList[i, j];
            }
        }
        scoreNumber.GetComponent<Score>().SetScore(score);
    }

    bool IsGameOver()
    {
        for (int j = size - 1; j >= 0; j--)
        {
            for (int i = 0; i < size; i++)
            {
                if (valueList[i, j] == 0)
                    return false;
            }
        }
        return true;
    }

    void LoadAll()
    {
        if (PlayerPrefs.HasKey("sn_size"))
        {
            int total = 0;
            string key;
            for (int j = 0; j < size; j++)
            {
                for (int i = 0; i < size; i++)
                {
                    key = "sn_value" + total;
                    if (PlayerPrefs.GetInt(key) > 0)
                        CreateNewBlock(PlayerPrefs.GetInt(key), j, i);
                    total++;
                }
            }
        }
    }

    void CheckSave()
    {
        if (PlayerPrefs.HasKey("sn_size"))
        {
            LoadAll();
            DeleteSave();
        }
        else
        {
            CreateNewBlock();
        }
    }

    void DeleteSave()
    {
        PlayerPrefs.DeleteKey("sn_size");
        for (int i = 0; i < 25; i++)
        {
            PlayerPrefs.DeleteKey("sn_value" + i);
        }
    }

    void LoadPrefs()
    {
        if (PlayerPrefs.HasKey("sn_delay"))
        {
            arrowsDelay = PlayerPrefs.GetInt("sn_delay");
        }

        if (PlayerPrefs.HasKey("sn_ai"))
        {
            if (PlayerPrefs.GetInt("sn_ai") == 1)
                enableArrows = true;
            else
                enableArrows = false;
        }
    }

    public void ToggleArrows(bool value)
    {
        if (enableArrows)
        {
            if (value)
            {
                Invoke("ArrowsOn", arrowsDelay);
            }
            else
            {
                arrows.SetActive(false);
            }
        }
    }

    void ArrowsOn()
    {
        if (enableArrows)
        {
            switch (action)
            {
                case 0:
                    arrows.transform.eulerAngles = new Vector3(0.0f, 0.0f, 270.0f);
                    break;
                case 1:
                    arrows.transform.eulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
                    break;
                case 2:
                    arrows.transform.eulerAngles = new Vector3(0.0f, 0.0f, 180.0f);
                    break;
                case 3:
                    arrows.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
                    break;
                default:
                    break;
            }
            arrows.SetActive(true);
        }
    }
}
