﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public GameObject musicButton;
    public GameObject particleButton;
    public GameObject delayButton;

    private GameObject music;
    private SoundManager soundManager;
    private Text musicText;
    private Text particleText;
    private Text delayText;

    void Awake()
    {
        music = GameObject.Find("Music");
    }

    void Start()
    {
        soundManager = music.GetComponent<SoundManager>();
        musicText = musicButton.GetComponentInChildren<Text>();
        particleText = particleButton.GetComponentInChildren<Text>();
        delayText = delayButton.GetComponentInChildren<Text>();

        if (PlayerPrefs.GetInt("sn_mute") == 1)
            musicText.text = "Music: off";
        else
            musicText.text = "Music: on";

        if (PlayerPrefs.GetInt("sn_particle") == 1)
            particleText.text = "Particles: on";
        else
            particleText.text = "Particles: off";

        delayText.text = "AI Delay: " + PlayerPrefs.GetInt("sn_delay") + " sec";
    }

    public void ToggleMusic()
    {
        if (PlayerPrefs.GetInt("sn_mute") == 1)
            MusicOn();
        else
            MusicOff();
    }

    void MusicOn()
    {
        musicText.text = "Music: on";
        soundManager.PlayMusic();
    }

    void MusicOff()
    {
        musicText.text = "Music: off";
        soundManager.StopMusic();
    }

    public void ToggleParticle()
    {
        if (PlayerPrefs.GetInt("sn_particle") == 1)
            ParticleOff();
        else
            ParticleOn();
    }

    void ParticleOn()
    {
        particleText.text = "Particles: on";
        PlayerPrefs.SetInt("sn_particle", 1);
    }

    void ParticleOff()
    {
        particleText.text = "Particles: off";
        PlayerPrefs.SetInt("sn_particle", 0);
    }

    public void ResetScore()
    {
        PlayerPrefs.DeleteKey("sn_highscore3");
        PlayerPrefs.DeleteKey("sn_highscore4");
        PlayerPrefs.DeleteKey("sn_highscore5");
    }

    public void ToggleDelay()
    {
        int delay = PlayerPrefs.GetInt("sn_delay");
        delay++;

        if (delay > 5)
            delay = 0;

        PlayerPrefs.SetInt("sn_delay", delay);
        delayText.text = "AI Delay: " + delay + " sec";
    }
}
