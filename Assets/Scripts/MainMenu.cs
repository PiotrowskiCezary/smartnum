﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject assistButton;
    private Text assistText;

    void Awake()
    {
        var a = transform.Find("ContinueButton");
        Button btn = a.gameObject.GetComponent<Button>();
        if (!PlayerPrefs.HasKey("sn_size"))
            btn.interactable = false;
    }

    void Start()
    {
        assistText = assistButton.GetComponentInChildren<Text>();

        if (!PlayerPrefs.HasKey("sn_mute"))
            PlayerPrefs.SetInt("sn_mute", 0);

        if (!PlayerPrefs.HasKey("sn_particle"))
            PlayerPrefs.SetInt("sn_particle", 1);

        if (!PlayerPrefs.HasKey("sn_delay"))
            PlayerPrefs.SetInt("sn_delay", 3);

        if (!PlayerPrefs.HasKey("sn_ai"))
        {
            PlayerPrefs.SetInt("sn_ai", 1);
        }
        else
        {
            if (PlayerPrefs.GetInt("sn_ai") == 1)
                assistText.text = "AI Assist: ON";
            else
                assistText.text = "AI Assist: OFF";
        }
    }

    public void Continue()
    {
        switch (PlayerPrefs.GetInt("sn_size"))
        {
            case 3:
                SceneManager.LoadScene("Game3x3");
                break;
            case 4:
                SceneManager.LoadScene("Game4x4");
                break;
            case 5:
                SceneManager.LoadScene("Game5x5");
                break;
            default:
                SceneManager.LoadScene("Game4x4");
                break;
        }
    }

    public void ToggleAssist()
    {
        if (PlayerPrefs.GetInt("sn_ai") == 1)
        {
            PlayerPrefs.SetInt("sn_ai", 0);
            assistText.text = "AI Assist: OFF";
        }
        else
        {
            PlayerPrefs.SetInt("sn_ai", 1);
            assistText.text = "AI Assist: ON";
        }
    }
}
