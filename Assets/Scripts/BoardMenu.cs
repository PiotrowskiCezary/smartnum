﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BoardMenu : MonoBehaviour
{
    public void Pick3()
    {
        DeleteSave();
        SceneManager.LoadScene("Game3x3");
    }

    public void Pick4()
    {
        DeleteSave();
        SceneManager.LoadScene("Game4x4");
    }

    public void Pick5()
    {
        DeleteSave();
        SceneManager.LoadScene("Game5x5");
    }

    void DeleteSave()
    {
        PlayerPrefs.DeleteKey("sn_size");
        for (int i = 0; i < 25; i++)
        {
            PlayerPrefs.DeleteKey("sn_value" + i);
        }
    }
}
