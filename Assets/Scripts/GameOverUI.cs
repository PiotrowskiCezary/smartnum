﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    public GameObject soundButton;
    public GameObject optionsButton;

    void Awake()
    {
        soundButton.GetComponent<Button>().interactable = false;
        optionsButton.GetComponent<Button>().interactable = false;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
