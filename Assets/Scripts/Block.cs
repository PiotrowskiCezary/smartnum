﻿using System.Collections;
using UnityEngine;

public class Block : MonoBehaviour
{
    public float speed = 5f;
    public Sprite[] objectSprites;
    public int previousValue = 0;
    public int value = 3;
    public GameObject particle;

    private bool isMoving = false;
    private Vector3 endPoint;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        ChangeText();
    }

    public void StartMoving(Vector3 endGoal)
    {
        endPoint = transform.position + endGoal;
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        if (isMoving)
            yield break;

        isMoving = true;
        while (MoveToPoint())
            yield return null;
        isMoving = false;
    }

    bool MoveToPoint()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPoint, speed * Time.deltaTime);

        if (endPoint == transform.position)
            return false;
        else
            return true;
    }

    void ChangeText()
    {
        FontSizeManager textManager = gameObject.GetComponentInChildren<FontSizeManager>();
        TextMesh valueShown = gameObject.GetComponentInChildren<TextMesh>();
        valueShown.text = value.ToString();

        if (previousValue.ToString().Length < value.ToString().Length)
        {
            textManager.TryRescale();
            ChangeColor();
        }
    }

    void ChangeColor()
    {
        int choice = value.ToString().Length;
        if (choice < 5)
            spriteRenderer.sprite = objectSprites[choice - 1];
    }

    void CreateParticle()
    {
        int choice = value.ToString().Length;
        Color color = new Color();
        switch (choice)
        {
            case 1:
                color = new Color(0f, 1f, 0.8980393f);
                break;
            case 2:
                color = new Color(0.6862745f, 1f, 0.9686275f);
                break;
            case 3:
                color = new Color(0f, 0.8313726f, 1f);
                break;
            case 4:
                color = new Color(0f, 0.627451f, 1f);
                break;
            default:
                color = new Color(0f, 0.4941177f, 1f);
                break;
        }

        GameObject p = Instantiate(particle, transform.position, Quaternion.identity) as GameObject;
        var particleSettings = p.GetComponent<ParticleSystem>().main;
        particleSettings.startColor = color;
    }

    void OnTriggerEnter2D(Collider2D targetObj)
    {
        if (targetObj.gameObject.tag == "Player")
        {
            if (isMoving)
            {
                if (value == targetObj.gameObject.GetComponent<Block>().value)
                {
                    previousValue = value;
                    value += targetObj.gameObject.GetComponent<Block>().value;
                    if (PlayerPrefs.GetInt("sn_particle") == 1)
                        CreateParticle();
                    Destroy(targetObj.gameObject);
                    ChangeText();
                }
            }
        }
    }
}