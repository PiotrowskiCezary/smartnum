﻿using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class AIAgent : Agent
{
    public GameObject board;
    private BoardManager boardManager;

    const int k_DoNothing = 4;
    const int k_Up = 1;
    const int k_Down = 0;
    const int k_Left = 2;
    const int k_Right = 3;

    public bool maskActions = true;

    void Start()
    {
        boardManager = board.GetComponent<BoardManager>();
    }

    public override void WriteDiscreteActionMask(IDiscreteActionMask actionMask)
    {
        if (maskActions)
        {
            if (!boardManager.CheckUp())
            {
                actionMask.WriteMask(0, new[] { k_Up });
            }

            if (!boardManager.CheckDown())
            {
                actionMask.WriteMask(0, new[] { k_Down });
            }

            if (!boardManager.CheckLeft())
            {
                actionMask.WriteMask(0, new[] { k_Left });
            }

            if (!boardManager.CheckRight())
            {
                actionMask.WriteMask(0, new[] { k_Right });
            }
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        for (int i = 0; i < boardManager.size; i++)
        {
            for (int j = 0; j < boardManager.size; j++)
            {
                sensor.AddObservation(boardManager.valueList[i, j]);
            }
        }
    }

    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        var pick = actionBuffers.DiscreteActions[0];
        switch (pick)
        {
            case k_Up:
                boardManager.action = pick;
                break;
            case k_Down:
                boardManager.action = pick;
                break;
            case k_Left:
                boardManager.action = pick;
                break;
            case k_Right:
                boardManager.action = pick;
                break;
            default:
                Debug.Log("Invalid action value!");
                break;
        }
        boardManager.ToggleArrows(true);
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var discreteActionsOut = actionsOut.DiscreteActions;
        discreteActionsOut[0] = k_DoNothing;
        if (Input.GetKey(KeyCode.W))
        {
            discreteActionsOut[0] = k_Up;
        }
        if (Input.GetKey(KeyCode.S))
        {
            discreteActionsOut[0] = k_Down;
        }
        if (Input.GetKey(KeyCode.A))
        {
            discreteActionsOut[0] = k_Left;
        }
        if (Input.GetKey(KeyCode.D))
        {
            discreteActionsOut[0] = k_Right;
        }
    }
}
